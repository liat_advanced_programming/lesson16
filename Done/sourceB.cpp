#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include "sqlite3.h"

using std::to_string;
using std::stoi;
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
using std::unordered_map;
using std::pair;

unordered_map<string, vector<string>> results;

/* Purchase function define queries */
#define SELECT_BUYER_ID "select id from buyers where id="
#define SELECT_CAR_ID "select id from cars where id="
#define SELECT_BALANCE "select balance from accounts where id="
#define SELECT_AVAILABE "select available from cars where id="
#define SELECT_PRICE "select price from cars where id="
#define UPDATE_BALANCE_1 "update accounts set balance="
#define UPDATE_BALANCE_2 " where id="
#define UPDATE_AVAILABE  "update cars set available=0 where id="
#define SELECT_CARS_DETAILS "select available, color, id from cars;"
#define SELECT_ID_FILTERED_BY_BALANCE "select id from accounts where balance>"

/* Functions Declaration */
int executePurchaseSql(string numberOfPurchase, string buyerId, string carId, sqlite3* db, char* zErrMsg);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg);
void printTable();
int callback(void* notUsed, int argc, char** argv, char** azCol);
void clearTable();


int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	/* Connection to the database */
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	
	/* (1) Create & Execute SQL statement */
	executePurchaseSql("1", "1", "1", db, zErrMsg);

	/* (2) Create & Execute SQL statement */
	executePurchaseSql("2", "9", "10", db, zErrMsg);

	/* (3) Create & Execute SQL statement */
	executePurchaseSql("3", "3", "12", db, zErrMsg);

	/* Transference */
	balanceTransfer(12, 2, 1000, db, zErrMsg);

	/* View cars details */
	string sql = SELECT_CARS_DETAILS;
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	cout << endl << "Cars details:" << endl;
	printTable();
	clearTable();
	
	int carId;
	cout << endl << "Choose car id to check who can buy it: ";
	cin >> carId;
	whoCanBuy(carId, db, zErrMsg);

	/* Close database */
	sqlite3_close(db);
	system("pause");
	return 0;
}

int executePurchaseSql(string numberOfPurchase, string buyerId, string carId, sqlite3* db, char* zErrMsg)
{
	string sql;
	int rc; 
	
	/* Create SQL & Excecute */
	cout << "(" + numberOfPurchase + ") ";
	sql = SELECT_BUYER_ID;
	sql.append(buyerId);
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	buyerId = results["id"][0];
	clearTable();

	sql = SELECT_CAR_ID;
	sql.append(carId);
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	carId = results["id"][0];
	clearTable();

	carPurchase(stoi(buyerId), stoi(carId), db, zErrMsg);
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	
	/* Buyer balance */
	string sql = SELECT_BALANCE;
	sql.append(to_string(buyerid));
	sql.append(";");
	int rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	string buyerBalance = results["balance"][0];
	clearTable();
	
	/* Car availability */
	sql = SELECT_AVAILABE;
	sql.append(to_string(carid));
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	string carAvailability = results["available"][0];
	clearTable();

	/* Check Car availability, exit if 0 */
	if (stoi(carAvailability) == 0)
	{
		cout << "| Purchase failed |" << endl;
		cout << "Car ";
		cout << carid;
		cout << " is not available" << endl;
		return false;
	}

	/* Car price */
	sql = SELECT_PRICE;
	sql.append(to_string(carid));
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	string carPrice = results["price"][0];
	clearTable();

	/* Check if the buyer has enough money to purchase the car, exit if negative */
	bool purchase = 0;
	if (stoi(buyerBalance) - stoi(carPrice) > 0)
	{
		purchase = 1;
	}
	else
	{
		cout << "| Purchase failed |" << endl;
		cout << buyerid;
		cout << " buyer has not enogh money in balance to purchase car ";
		cout << carid << endl;
		return false;
	}

	/* Commit Purchase */
	if (purchase)
	{
		/* Remove car's price from buyer's account */
		sql = UPDATE_BALANCE_1;
		sql.append(to_string(stoi(buyerBalance) - stoi(carPrice)));
		sql.append(UPDATE_BALANCE_2);
		sql.append(to_string(buyerid));
		sql.append(";");
		rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);

		/* Change car availability */
		sql = UPDATE_AVAILABE;
		sql.append(to_string(carid));
		sql.append(";");
		rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);

		cout << "| Purchase comitted |" << endl;
		cout << "Buyer ";
		cout << buyerid;
		cout << " purchased the ";
		cout << carid;
		cout << " car" << endl;
	}
	clearTable();
	return true;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	string sql;
	int rc;

	cout << endl << "Transference: ";
	cout << "Buyer ";
	cout << from;
	cout << " wants to transfer ";
	cout << amount;
	cout << "$ to buyer ";
	cout << to << endl;

	/* Get balance - transfer from */
	sql = SELECT_BALANCE;
	sql.append(to_string(from));
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	int fromBalance = stoi(results["balance"][0]);
	clearTable();

	/* Get balance - transfer to */
	sql = SELECT_BALANCE;
	sql.append(to_string(to));
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	int toBalance = stoi(results["balance"][0]);
	clearTable();

	if (fromBalance < amount)
	{
		cout << "Error: not enough money in balance to commit the transference" << endl;
		return false;
	}
	
	/* commit transference */
	sql = UPDATE_BALANCE_1;
	sql.append(to_string(fromBalance - amount));
	sql.append(UPDATE_BALANCE_2);
	sql.append(to_string(from));
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);

	sql = UPDATE_BALANCE_1;
	sql.append(to_string(toBalance + amount));
	sql.append(UPDATE_BALANCE_2);
	sql.append(to_string(to));
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	
	cout << "Transference commited successfully" << endl;
	return true;
}

void whoCanBuy(int carId, sqlite3 * db, char * zErrMsg)
{
	string sql;
	int rc;

	/* Car price */
	sql = SELECT_PRICE;
	sql.append(to_string(carId));
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	string carPrice = results["price"][0];
	clearTable();

	cout << "Buyers id's who can buy the chosen car: " << endl;

	sql = SELECT_ID_FILTERED_BY_BALANCE;
	sql.append(carPrice);
	sql.append(";");
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	printTable();
	clearTable();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}
