#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include "sqlite3.h"

using std::string;
using std::vector;
using std::unordered_map;
using std::pair;

unordered_map<string, vector<string>> results;

#define CREATE_TABLE "create table people(id integer primary key autoincrement, name string);"
#define INSERT_1ST_PERSON "insert into people(name) values(\"Mohana\");"
#define INSERT_2ND_PERSON "insert into people(name) values(\"Pocahontas\");"
#define INSERT_3RD_PERSON "insert into people(name) values(\"Mulan\");"
#define UPDATE_NAME "update people set name=\"Bell\" where name=\"Mohana\";"

int dbBuild();
int callback(void* notUsed, int argc, char** argv, char** azCol);

int main()
{
	dbBuild();
	system("Pause");
	return 0;
}


int dbBuild()
{
	int rc;
	string sql;
	sqlite3* db;
	char *zErrMsg = 0;
	
	/* Connection to the database */
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	/* Create & Execute SQL statements */
	sql = CREATE_TABLE;
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	sql = INSERT_1ST_PERSON;
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	sql = INSERT_2ND_PERSON;
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	sql = INSERT_3RD_PERSON;
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);
	sql = UPDATE_NAME;
	rc = sqlite3_exec(db, sql.c_str(), callback, nullptr, &zErrMsg);

	/* Close database */
	sqlite3_close(db);
	return 0;
}


int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

